####
## Residential
####

IO_plan<-function(data, plan,
                  tz="AEST"){
  #Requires peak_analysis
  if("everything"%in%plan){
    plan<-c("Spark","Millennium","Lightning",
            "Jupiter","Pulse","Pulse Plus")
  }
  
  data<-data%>%
    mutate(
      ToU=peak_analysis(.data[[tz]]),
      month=month(.data[[tz]]),
      net_export=-1*pmin(Total,0),
      deficit=pmax(Total,0))
  
  n_days<-n_distinct(data$SettlementDate)
  n_range<-interval(range(data[[tz]])[1],
                    range(data[[tz]])[2])
  n_months<-round(as.numeric(
    as.duration(n_range),
    "months"
  ))
  
  spark<-function(){
    daily_supply<-1.1
    ctrl_load<-0.18
    solar_fit<-0.08
    charge_24hrs<-0.30
    
    rates<-c(
      Supply=daily_supply,
      OPCL=ctrl_load,
      Usage=charge_24hrs,
      `Solar Revenue`=solar_fit
    )
    
    data<-data%>%
      mutate(OPCL.adj=pmax(deficit-Main,0))%>%
      mutate(Main.adj=deficit-OPCL.adj)%>%
      summarise(Supply=n_days,
                OPCL=sum(OPCL.adj),
                Usage=sum(Main.adj),
                `Solar Revenue`=sum(net_export))%>%
      pivot_longer(everything(),
                   names_to="Component",
                   values_to="Volume (kW)")%>%
      mutate(Rate=rates[Component])%>%
      mutate(`Cost ($)`=`Volume (kW)`*Rate)
    return(data%>%
             arrange(match(Component,names(rates))))
  }
  
  millennium<-function(){
    daily_supply<-1.1
    metering<-11
    solar_fit<-0.08
    charge_offpeak<-0.18
    charge_solarspg<-0.12
    charge_others<-0.36
    
    rates<-c(
      Supply=daily_supply,
      Metering=metering,
      `Usage Offpeak`=charge_offpeak,
      `Usage Solar`=charge_solarspg,
      `Usage Peak`=charge_others,
      `Solar Revenue`=solar_fit
    )
    
    cost_analysis<-function(ToU){
      case_when(
        ToU=="Usage Off-Peak"~"Usage Offpeak",
        ToU=="Solar Sponge"~"Usage Solar",
        TRUE~"Usage Peak")
    }
    
    usage_summary<-data%>%
      mutate(Component=cost_analysis(ToU))%>%
      group_by(Component)%>%
      summarise(`Volume (kW)`=sum(deficit),
                .groups="drop")
    
    data<-data%>%
      summarise(Supply=n_days,
                Metering=n_months,
                `Solar Revenue`=sum(net_export))%>%
      pivot_longer(everything(),
                   names_to="Component",
                   values_to="Volume (kW)")%>%
      bind_rows(usage_summary)%>%
      mutate(Rate=rates[Component])%>%
      mutate(`Cost ($)`=`Volume (kW)`*Rate)
    return(data%>%
             arrange(match(Component,names(rates))))
  }
  
  lightning<-function(){
    daily_supply<-1.1
    metering<-11
    solar_fit<-0.06
    charge_offpeak<-0.12
    charge_solarspg<-0.08
    charge_shoulder<-0.20
    charge_peak.summer<-1.25
    charge_peak.winter<-0.36
    winter<-seq(4,10,by=1)
    
    rates<-c(
      Supply=daily_supply,
      Metering=metering,
      `Usage Offpeak`=charge_offpeak,
      `Usage Solar`=charge_solarspg,
      `Usage Shoulder`=charge_shoulder,
      `Usage Peak Summer`=charge_peak.summer,
      `Usage Peak Winter`=charge_peak.winter,
      `Solar Revenue`=solar_fit
    )
    
    cost_analysis<-function(ToU,month){
      case_when(
        ToU=="Usage Off-Peak"~"Usage Offpeak",
        ToU=="Solar Sponge"~"Usage Solar",
        ToU=="Usage Shoulder"~"Usage Shoulder",
        TRUE~if_else(
          month%in%winter,
          "Usage Peak Winter",
          "Usage Peak Summer"
        )
      )
    }
    
    usage_summary<-data%>%
      mutate(Component=cost_analysis(ToU,month))%>%
      group_by(Component)%>%
      summarise(`Volume (kW)`=sum(deficit),
                .groups="drop")
    
    data<-data%>%
      summarise(Supply=n_days,
                Metering=n_months,
                `Solar Revenue`=sum(net_export))%>%
      pivot_longer(everything(),
                   names_to="Component",
                   values_to="Volume (kW)")%>%
      bind_rows(usage_summary)%>%
      mutate(Rate=rates[Component])%>%
      mutate(`Cost ($)`=`Volume (kW)`*Rate)
    return(data%>%
             arrange(match(Component,names(rates))))
  }
  
  jupiter<-function(){
    daily_supply<-1.1
    metering<-11
    solar_fit<-0.06
    charge_offpeak<-0.12
    charge_solarspg<-0.08
    charge_others<-0.20
    charge_peak.summer<-50
    charge_peak.winter<-8
    winter<-seq(4,10,by=1)
    
    rates<-c(
      Supply=daily_supply,
      Metering=metering,
      `Usage Offpeak`=charge_offpeak,
      `Usage Solar`=charge_solarspg,
      `Usage Peak`=charge_others,
      `Demand Peak Summer`=charge_peak.summer,
      `Demand Peak Winter`=charge_peak.winter,
      `Solar Revenue`=solar_fit
    )
    
    cost_analysis<-function(ToU){
      case_when(
        ToU=="Usage Off-Peak"~"Usage Offpeak",
        ToU=="Solar Sponge"~"Usage Solar",
        TRUE~"Usage Peak")
    }
    
    Demand_Charge<-data%>%
      filter(ToU=="Usage Peak")%>%
      group_by(SettlementDate)%>%
      mutate(peak_avg=mean(deficit)*2, #Convert to Hours
             year=year(.data[[tz]]))%>%
      group_by(year,month)%>%
      summarise(max_avg=max(peak_avg),
                .groups="drop")%>%
      mutate(Component=case_when(
        month%in%winter~"Demand Peak Winter",
        TRUE~"Demand Peak Summer"
      ))%>%
      group_by(Component)%>%
      summarise(`Volume (kW)`=sum(max_avg),
                .groups="drop")
    
    usage_summary<-data%>%
      mutate(Component=cost_analysis(ToU))%>%
      group_by(Component)%>%
      summarise(`Volume (kW)`=sum(deficit),
                .groups="drop")
    
    data<-data%>%
      summarise(Supply=n_days,
                Metering=n_months,
                `Solar Revenue`=sum(net_export))%>%
      pivot_longer(everything(),
                   names_to="Component",
                   values_to="Volume (kW)")%>%
      bind_rows(usage_summary,
                Demand_Charge)%>%
      mutate(Rate=rates[Component])%>%
      mutate(`Cost ($)`=`Volume (kW)`*Rate)
    return(data%>%
             arrange(match(Component,names(rates))))
  }
  
  pulse<-function(){
    daily_supply<-1.6
    ctrl_load<-0.19
    solar_fit<-0.08
    charge_24hrs<-0.31
    
    rates<-c(
      Supply=daily_supply,
      OPCL=ctrl_load,
      Usage=charge_24hrs,
      `Solar Revenue`=solar_fit
    )
    
    data<-data%>%
      mutate(OPCL.adj=pmax(deficit-Main,0))%>%
      mutate(Main.adj=deficit-OPCL.adj)%>%
      summarise(Supply=n_days,
                OPCL=sum(OPCL.adj),
                Usage=sum(Main.adj),
                `Solar Revenue`=sum(net_export))%>%
      pivot_longer(everything(),
                   names_to="Component",
                   values_to="Volume (kW)")%>%
      mutate(Rate=rates[Component])%>%
      mutate(`Cost ($)`=`Volume (kW)`*Rate)
    return(data%>%
             arrange(match(Component,names(rates))))
  }
  
  pulse_plus<-function(){
    daily_supply<-1.6
    ctrl_load<-0.19
    solar_fit<-0.08
    charge_work.hrs<-0.33
    charge_others<-0.24
    workdays<-seq(2,6,by=1)
    workhours<-seq(7,20,by=1)
    
    rates<-c(
      Supply=daily_supply,
      OPCL=ctrl_load,
      `Usage Work Hours`=charge_work.hrs,
      `Usage Non-Work`=charge_others,
      `Solar Revenue`=solar_fit
    )
    
    data<-data%>%
      mutate(OPCL.adj=pmax(deficit-Main,0))%>%
      mutate(Main.adj=deficit-OPCL.adj)
    
    usage_summary<-data%>%
      mutate(wday=wday(.data[[tz]],
                       week_start = #Default 1st day is Sunday
                         getOption("lubridate.week.start", 7)))%>%
      mutate(Component=case_when(
        ((wday%in%workdays)&(hour(.data[[tz]])%in%workhours))~
          "Usage Work Hours",
        TRUE~"Usage Non-Work"
      ))%>%
      group_by(Component)%>%
      summarise(`Volume (kW)`=sum(Main.adj),
                .groups="drop")
    
    data<-data%>%
      summarise(Supply=n_days,
                OPCL=sum(OPCL.adj),
                `Solar Revenue`=sum(net_export))%>%
      pivot_longer(everything(),
                   names_to="Component",
                   values_to="Volume (kW)")%>%
      bind_rows(usage_summary)%>%
      mutate(Rate=rates[Component])%>%
      mutate(`Cost ($)`=`Volume (kW)`*Rate)
    return(data%>%
             arrange(match(Component,names(rates))))
  }
  
  switch<-function(plan){
    type<-case_when(
      plan=="Spark"~"spark",
      plan=="Millennium"~"millennium",
      plan=="Lightning"~"lightning",
      plan=="Jupiter"~"jupiter",
      plan=="Pulse"~"pulse",
      plan=="Pulse Plus"~"pulse_plus"
    )
    do.call(type,args=list())
  }
  
  output<-set_names(plan)%>%
    map(.,switch)
  return(output)
}

####
## Large Business
####

LB_plan<-function(Elist,
                  Qlist,
                  ID.data,
                  tz="AEST"){
  #Requires function merged_sum
  #Requires function read.dst
  #Requires zoo function rollapply
  
  export.kWh<-merged_sum(Elist)%>%
    read.dst()%>%
    transmute(SettlementDate=date(.data[[tz]]),
              .data[[tz]],
              Demand=pmax(Demand,0))
  export.kVA<-merged_sum(Qlist)%>%
    read.dst()%>%
    transmute(SettlementDate=date(.data[[tz]]),
              .data[[tz]],
              Demand=pmax(Demand,0))
  
  if(any(range(export.kWh[[tz]])!=
         range(export.kVA[[tz]]))){
    stop("data frames are not of the same time intervals")
  }
  
  n_days<-n_distinct(export.kWh$SettlementDate)
  n_range<-interval(range(export.kWh[[tz]])[1],
                    range(export.kWh[[tz]])[2])
  n_months<-round(as.numeric(
    as.duration(n_range),
    "months"
  ))
  n_meter<-length(unique(ID.data$meter.id))
  
  energy_rates<-c(
    `Energy: Daytime Off-Peak`=0.02,
    `Energy: Night Off-Peak`=0.04,
    `Energy: Usage Shoulder`=0.05,
    `Energy: Usage Peak`=0.1
  )
  energy_labels<-names(energy_rates)
  energy_ToU<-function(tz){
    energy_labels
    x<-hour(tz)
    day_offpeak<-9:14
    night_offpeak<-1:5
    peak<-17:20
    y<-case_when(
      x%in%day_offpeak~energy_labels[1],
      x%in%night_offpeak~energy_labels[2],
      x%in%peak~energy_labels[4],
      TRUE~energy_labels[3]
    )
    return(y)
  }
  
  network_rates<-c(
    `Network: Off-Peak Energy`=0.0414,
    `Network: Peak Energy`=0.0662
  )
  network_labels<-names(network_rates)
  network_ToU<-function(tz){
    workhours<-7:20
    workdays<-2:6
    y<-case_when(
      ((wday(tz,week_start = #Default 1st day is Sunday
               getOption("lubridate.week.start", 7))
        %in%workdays)&
         (hour(tz)%in%workhours)
      )~network_labels[2],
      TRUE~network_labels[1]
    )
    return(y)
  }
  network_tariff<-c(
    `Network: Peak Demand`=0.2533,
    `Network: Anytime Demand`=0.1036
  )
  
  solar<-c(
    peak=0.09,
    offpeak=0.02
  )
  winter<-seq(4,10,by=1)
  
  energy_summary<-export.kWh%>%
    mutate(Component=energy_ToU(.data[[tz]]))%>%
    group_by(Component)%>%
    summarise(Volume=sum(Demand),
              .groups="drop")%>%
    mutate(Unit="$/kWh",
           Rate=energy_rates[Component])
  
  network_summary<-export.kWh%>%
    mutate(Component=network_ToU(.data[[tz]]))%>%
    group_by(Component)%>%
    summarise(Volume=sum(Demand),
              .groups="drop")%>%
    mutate(Unit="$/kWh",
           Rate=network_rates[Component])
  
  anytime_demand<-export.kVA%>%
    group_by(SettlementDate)%>%
    summarise(daily_max=max(Demand),
              .groups="drop")%>%
    mutate(Anytime=rollapply(daily_max,365,max,
                             align="right",
                             partial=T))%>%
    summarise(Volume=sum(Anytime),
              .groups="drop")
  
  peak_tariff.data<-export.kVA%>%
    filter(hour(.data[[tz]])%in%seq(17,20,by=1))%>%
    group_by(SettlementDate)%>%
    summarise(avg=mean(Demand),
              .groups="drop")
  
  peak_tariff.vector<-peak_tariff.data%>%
    mutate(month=month(SettlementDate),
           year=year(SettlementDate))%>%
    mutate(avg=if_else(month%in%winter,
                       0,
                       avg))%>%
    mutate(max_avg=rollapply(avg,365,max,
                             align="right",
                             partial=T))
  peak_start.row<-which(!month(peak_tariff.data$SettlementDate)
                        %in%winter)%>%
    first()
  if(peak_start.row!=1){
    peak_tariff.vector$max_avg[1:(peak_start.row-1)]<-
      cummax(peak_tariff.data$avg[1:(peak_start.row-1)])
  }
  
  peak_demand<-peak_tariff.vector%>%
    summarise(Volume=sum(max_avg),
              .groups="drop")
  
  energy_tariff<-c(
    `Energy: Peak Demand`=0.1,
    `Energy: Anytime Demand`=0.01
  )
  energy_peak.summary<-peak_demand%>%
    transmute(Component=names(energy_tariff)[1],
              Volume,
              Unit="$/kVA/day")%>%
    mutate(Rate=energy_tariff[Component])
  energy_anytime.summary<-anytime_demand%>%
    transmute(Component=names(energy_tariff)[2],
              Volume,
              Unit="$/kVA/day")%>%
    mutate(Rate=energy_tariff[Component])
  
  network_peak.summary<-peak_demand%>%
    transmute(Component=names(network_tariff)[1],
              Volume,
              Unit="$/kVA/day")%>%
    mutate(Rate=network_tariff[Component])
  network_anytime.summary<-anytime_demand%>%
    transmute(Component=names(network_tariff)[2],
              Volume,
              Unit="$/kVA/day")%>%
    mutate(Rate=network_tariff[Component])
  
  
  flat_data<-export.kWh%>%
    summarise(Volume=sum(Demand),
              .groups="drop")
  
  env_charge<-c(
    `Environmental: SRES`=0.004,
    `Environmental: LRET`=0.004
  )
  SRES.summary<-flat_data%>%
    transmute(Component=names(env_charge)[1],
              Volume,
              Unit="$/kWh")%>%
    mutate(Rate=env_charge[Component])
  LRET.summary<-flat_data%>%
    transmute(Component=names(env_charge)[2],
              Volume,
              Unit="$/kWh")%>%
    mutate(Rate=env_charge[Component])
  
  market_charge<-c(
    `Market: AEMO`=0.0004,
    `Market: AEMO Ancillery`=0.002
  )
  AEMO.summary<-flat_data%>%
    transmute(Component=names(market_charge)[1],
              Volume,
              Unit="$/kWh")%>%
    mutate(Rate=market_charge[Component])
  ancillery.summary<-flat_data%>%
    transmute(Component=names(market_charge)[2],
              Volume,
              Unit="$/kWh")%>%
    mutate(Rate=market_charge[Component])
  
  other<-c(
    `Network: Standing Charge`=6.8493,
    `Metering Charge`=60,
    `Retail Charge`=150
  )
  
  standing.summary<-tibble(
    Component=names(other)[1],
    Volume=n_days,
    Unit="$/day"
  )%>%
    mutate(Rate=other[Component])
  
  metering.summary<-tibble(
    Component=names(other)[2],
    Volume=n_meter*n_months,
    Unit="$/meter/month"
  )%>%
    mutate(Rate=other[Component])
  
  retail.summary<-tibble(
    Component=names(other)[3],
    Volume=n_months,
    Unit="$/month"
  )%>%
    mutate(Rate=other[Component])
  
  output<-bind_rows(energy_summary,
                    energy_peak.summary,
                    energy_anytime.summary,
                    network_summary,
                    network_peak.summary,
                    network_anytime.summary,
                    standing.summary,
                    SRES.summary,
                    LRET.summary,
                    AEMO.summary,
                    ancillery.summary,
                    metering.summary,
                    retail.summary
  )
  
  loss.factor<-function(Component,Rate){
    names<-c(
      energy_labels,
      names(env_charge),
      names(market_charge)
    )
    DLF<-1.1
    y<-case_when(
      Component%in%names~Rate*1.1,
      TRUE~Rate
    )
    return(y)
  }
  
  output<-output%>%
    mutate(`Rate w Loss`=loss.factor(Component = Component,
                                     Rate = Rate))%>%
    mutate(Cost=Volume*`Rate w Loss`)
  
  return(output%>%
           relocate(Unit,
                    .after="Rate w Loss"))
}